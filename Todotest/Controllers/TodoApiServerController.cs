﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Todotest.Models;// import models
using System.Data.SqlClient;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.ApplicationInsights;

namespace Todotest.Controllers
{
    [Route("todolist")]
    [EnableCors("any")]

    public class TodoApiServerController:Controller
    {
        private static readonly string sql_server = "cabchrage-todotest.database.windows.net";
        private static readonly string sql_username = "Todotest";
        private static readonly string sql_password = "cabcharge123#";
        private static readonly string sql_sdb = "todotest-db";
        private TelemetryClient telemetry = new TelemetryClient();

        [HttpGet]
        [Route("Todos")]
        public IEnumerable<TodoTask> Todos()
        {
            List<TodoTask> tasks = new List<TodoTask>();//tasks is list with the model of TodoTask

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = sql_server;
            builder.UserID = sql_username;
            builder.Password = sql_password;
            builder.InitialCatalog = sql_sdb;

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT TASK_ID, TASK_NAME FROM TODO_LIST;");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tasks.Add(new TodoTask(reader.GetDecimal(0), reader.GetString(1)));
                        }
                    }
                }
            }
            telemetry.TrackEvent("WinGame");
            var ex1 = new Exception("new error");
            telemetry.TrackException(ex1);
            return tasks.ToList();//return IEnumerable
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Create([FromBody]JObject value)
        {
            TodoTask posted = value.ToObject<TodoTask>();

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = sql_server;
            builder.UserID = sql_username;
            builder.Password = sql_password;
            builder.InitialCatalog = sql_sdb;

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                StringBuilder sb = new StringBuilder();
                sb.Append("INSERT INTO TODO_LIST (TASK_NAME,TASK_ID) VALUES ('" + posted.Task_Name + "'," + posted.Task_ID + ");");
                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();
                }
            }

            TodoTask todo = GetTodoTaskByName(posted.Task_Name);
            return new ObjectResult(todo); ;
        }

        private TodoTask GetTodoTaskByName(String name)
        {

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = sql_server;
            builder.UserID = sql_username;
            builder.Password = sql_password;
            builder.InitialCatalog = sql_sdb;

            TodoTask todo = new TodoTask();

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();
                String sql = ("SELECT TASK_ID, TASK_NAME FROM TODO_LIST WHERE TASK_NAME = '" + name + "' ORDER BY TASK_ID DESC;");

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            todo.Task_ID = reader.GetDecimal(0);
                            todo.Task_Name = reader.GetString(1);
                            break;
                        }
                    }
                }
            }

            return todo;
        }
    }
}
