﻿using System;
namespace Todotest.Models
{
    public class TodoTask
    {
        public TodoTask()
        {
        }

        public TodoTask(decimal Task_ID, string Task_Name)
        {
            this.Task_ID = Task_ID;
            this.Task_Name = Task_Name;
        }

        public decimal Task_ID { get; set; }
        public string Task_Name { get; set; }
    }
}
